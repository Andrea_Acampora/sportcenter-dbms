-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec 19 2018              
-- * Generation date: Thu Jun 11 22:36:16 2020 
-- * LUN file: /home/arop/UNI/IT/DATABASE/PROGETTO/sportcenter-dbms/database.lun 
-- * Schema: CENTRO_SPORTIVO/LOGICO 
-- ********************************************* 


-- Database Section
-- ________________

DROP DATABASE IF EXISTS CENTRO_SPORTIVO;
CREATE DATABASE IF NOT EXISTS CENTRO_SPORTIVO;
USE CENTRO_SPORTIVO;

# ---------------------------------------------------------------------- #
# Add table "ABBONAMENTI"                                                #
# ---------------------------------------------------------------------- #
CREATE TABLE `ABBONAMENTI` (
     `durata` INTEGER NOT NULL,
     `codiceAbbonamento` INTEGER NOT NULL AUTO_INCREMENT,
     `dataSottoscrizione` DATE NOT NULL,
     `inizioValidita` DATE NOT NULL,
     `codiceFiscale_Iscritto` VARCHAR(16) NOT NULL,
     `codiceCorso` VARCHAR(5) not null,
     CONSTRAINT `IDABBONAMENTO` PRIMARY KEY (`codiceAbbonamento`));

# ---------------------------------------------------------------------- #
# Add table "ACQUISTI"                                                   #
# ---------------------------------------------------------------------- #
create table `ACQUISTI` (
     `codiceAcquisto` INTEGER not null AUTO_INCREMENT,
     `codiceTipologia` VARCHAR(5) not null,
     `codiceArticolo` VARCHAR(6) not null,
     `data` DATE not null,
     `puntiGuadagnati` INTEGER not null,
     `codiceFiscale` VARCHAR(16) not null,
     constraint `IDACQUISTO` primary key (`codiceAcquisto`));

# ---------------------------------------------------------------------- #
# Add table "ADDETTI_PULIZIE"                                            #
# ---------------------------------------------------------------------- #
create table `ADDETTI_PULIZIE` (
     `codiceFiscale` VARCHAR(16) not null,
     `nome` VARCHAR(20) not null,
     `cognome` VARCHAR(20) not null,
     `telefono` VARCHAR(15) not null,
     `mail` VARCHAR(30) not null,
     `pagaOraria` INTEGER not null,
     constraint `IDADDETTI_PULIZIE` primary key (`codiceFiscale`));

# ---------------------------------------------------------------------- #
# Add table "ALLENATORI"                                                 #
# ---------------------------------------------------------------------- #
create table `ALLENATORI` (
     `codiceFiscale` VARCHAR(16) not null,
     `nome` VARCHAR(20) not null,
     `cognome` VARCHAR(20) not null,
     `telefono` VARCHAR(15) not null,
     `mail` VARCHAR(30) not null,
     `pagaOraria` INTEGER not null,
     `gradoPatentino` VARCHAR(1) not null,
     constraint `IDALLENATORE` primary key (`codiceFiscale`));

# ---------------------------------------------------------------------- #
# Add table "ALLENATORI_CORSI"                                           #
# ---------------------------------------------------------------------- #
create table `ALLENATORI_CORSI` (
     `codiceFiscale_Allenatore` VARCHAR(16) not null,
     `codiceCorso` VARCHAR(5) not null,
     constraint `IDALLENATORI_CORSI` primary key (`codiceFiscale_Allenatore`, `codiceCorso`));


# ---------------------------------------------------------------------- #
# Add table "ARMADIETTI"                                                 #
# ---------------------------------------------------------------------- #
create table `ARMADIETTI` (
     `idSpogliatoio` VARCHAR(3) not null,
     `numero` INTEGER not null,
     constraint `IDARMADIETTO` primary key (`idSpogliatoio`, `numero`));


# ---------------------------------------------------------------------- #
# Add table "ARTICOLI_SPORTIVI"                                          #
# ---------------------------------------------------------------------- #
create table `ARTICOLI_SPORTIVI` (
     `codiceTipologia` VARCHAR(10) not null,
     `codice_articolo` VARCHAR(6) not null,
     `taglia` VARCHAR(4) not null,
     constraint `IDARTICOLO_SPORTIVO_ID` primary key (`codiceTipologia`, `codice_articolo`));


# ---------------------------------------------------------------------- #
# Add table "CAMPI"                                                      #
# ---------------------------------------------------------------------- #
create table `CAMPI` (
     `codiceCampo` VARCHAR(3) not null,
     `superficie` INTEGER not null,
     `costoOrario` INTEGER not null,
     `tipologia_campo` VARCHAR(10) not null,
     `idSpogliatoio` VARCHAR(3) not null,
     constraint `IDCAMPO` primary key (`codiceCampo`));


# ---------------------------------------------------------------------- #
# Add table "CORSI"                                                      #
# ---------------------------------------------------------------------- #
create table `CORSI` (
     `codiceCorso` VARCHAR(5) not null,
     `codiceSport` VARCHAR(5) not null,
     `descrizione` VARCHAR(20) not null,
     `eta_partecipanti` INTEGER not null,
     `max_partecipanti` INTEGER not null ,
     constraint `IDCORSO` primary key (`codiceCorso`));


# ---------------------------------------------------------------------- #
# Add table "FASCE_ORARIE"                                               #
# ---------------------------------------------------------------------- #
create table `FASCE_ORARIE` (
     `idFascia` VARCHAR(5) not null,
     `ora_inizio` INTEGER not null,
     `ora_fine` INTEGER not null,
     constraint `IDFASCIA_ORARIA` primary key (`idFascia`));


# ---------------------------------------------------------------------- #
# Add table "FISIOTERAPISTI"                                             #
# ---------------------------------------------------------------------- #
create table `FISIOTERAPISTI` (
     `codiceFiscale` VARCHAR(16) not null,
     `nome` VARCHAR(20) not null,
     `cognome` VARCHAR(20) not null,
     `telefono` VARCHAR(15) not null,
     `mail` VARCHAR(30) not null,
     `pagaOraria` INTEGER not null,
     `specializzazione` VARCHAR(20) not null,
     `annoAbilitazione` INTEGER not null,
     constraint `IDFISIOTERAPISTA` primary key (`codiceFiscale`));


# ---------------------------------------------------------------------- #
# Add table "INTERVENTI_PULIZIA"                                         #
# ---------------------------------------------------------------------- #
create table `INTERVENTI_PULIZIA` (
     `idSpogliatoio` VARCHAR(3) not null,
     `codiceFiscale_Addetto` VARCHAR(16) not null,
     `durata` INTEGER not null,
     `data` DATE not null,
     constraint `IDINTERVENTO_PULIZIA` primary key (`idSpogliatoio`, `codiceFiscale_Addetto`,`data`));


# ---------------------------------------------------------------------- #
# Add table "ISCRITTI"                                                      #
# ---------------------------------------------------------------------- #
create table `ISCRITTI` (
     `codiceFiscale` VARCHAR(16) not null,
     `nome` VARCHAR(20) not null,
     `cognome` VARCHAR(20) not null,
     `telefono` VARCHAR(15) not null,
	 `dataNascita` DATE not null,
     `mail` VARCHAR(30) not null,
     constraint `IDISCRITTO_ID` primary key (`codiceFiscale`));


# ---------------------------------------------------------------------- #
# Add table "OCCUPAZIONI_CAMPI"                                          #
# ---------------------------------------------------------------------- #
create table `OCCUPAZIONI_CAMPI` (
     `codiceCampo` VARCHAR(3) not null,
     `idFascia` VARCHAR(5) not null,
     `data` date not null,
     `codiceCorso` VARCHAR(5),
     `codiceFiscale_Iscritto` VARCHAR(16),
     constraint `IDOCCUPAZIONE_CAMPO` primary key (`codiceCampo`, `idFascia`, `data`));


# ---------------------------------------------------------------------- #
# Add table "SEDUTE"                                                     #
# ---------------------------------------------------------------------- #
create table `SEDUTE` (
     `codiceFiscale_Fisioterapista` VARCHAR(16) not null,
     `codiceFiscale_Iscritto` VARCHAR(16) not null,
     `durata` INTEGER not null,
     `data` DATE not null,
     `esito` VARCHAR(20) not null,
     constraint `IDseduta` PRIMARY KEY (`codiceFiscale_Fisioterapista`, `codiceFiscale_Iscritto`,`data`));


# ---------------------------------------------------------------------- #
# Add table "SPOGLIATOI"                                                 #
# ---------------------------------------------------------------------- #
create table `SPOGLIATOI` (
     `idSpogliatoio` VARCHAR(3) not null,
     `superficie` INTEGER not null,
     `numDocce` INTEGER not null,
     constraint `IDSPOGLIATOIO` primary key (`idSpogliatoio`));


# ---------------------------------------------------------------------- #
# Add table "SPORT"                                                      #
# ---------------------------------------------------------------------- #
create table `SPORT` (
     `codiceSport` varchar(5) not null,
     `nome` varchar(20) not null,
     constraint `IDSPORT` primary key (`codiceSport`));


# ---------------------------------------------------------------------- #
# Add table "TESSERE"                                                    #
# ---------------------------------------------------------------------- #
create table `TESSERE` (
     `numTessera` integer not null AUTO_INCREMENT,
     `codiceFiscale_Iscritto` varchar(16) not null,
     `punti` integer not null DEFAULT 0,
     constraint `IDTESSERA` primary key (`numTessera`));


# ---------------------------------------------------------------------- #
# Add table "TIPOLOGIE_ABBONAMENTI"                                      #
# ---------------------------------------------------------------------- #
create table `TIPOLOGIE_ABBONAMENTI` (
     `durata` INTEGER  not null,
     `prezzo` INTEGER not null,
     `puntiOttenuti` INTEGER not null,
     constraint `IDTIPOLOGIA_ABB` primary key (`durata`));


# ---------------------------------------------------------------------- #
# Add table "TIPOLOGIE_PRODOTTI"                                         #
# ---------------------------------------------------------------------- #
create table `TIPOLOGIE_PRODOTTI` (
     `codiceTipologia` VARCHAR(5) not null,
     `prezzo` INTEGER not null,
     `descrizione` VARCHAR(30) not null,
     constraint `IDMATERIALE_SPORTIVO` primary key (`codiceTipologia`));




-- Constraints Section
-- ___________________ 

alter table `ABBONAMENTI` add constraint `FKtipologia`
     foreign key (`durata`)
     references `TIPOLOGIE_ABBONAMENTI` (`durata`);

alter table `ABBONAMENTI` add constraint `FKsottoscrive`
     foreign key (`codiceFiscale_Iscritto`)
     references `ISCRITTI` (`codiceFiscale`);
     
alter table `ABBONAMENTI` add constraint `FKcomprende`
     foreign key (`codiceCorso`)
     references `CORSI` (`codiceCorso`);

alter table `ACQUISTI` add constraint `FKriferito_FK`
     foreign key (`codiceTipologia`, `codiceArticolo`)
     references `ARTICOLI_SPORTIVI` (`codiceTipologia`, `codice_articolo`);

alter table `ACQUISTI` add constraint `FKesegue`
     foreign key (`codiceFiscale`)
     references `ISCRITTI` (`codiceFiscale`);

alter table `ALLENATORI_CORSI` add constraint `FKallenare`
     foreign key (`codiceFiscale_Allenatore`)
     references `ALLENATORI` (`codiceFiscale`);

alter table `ALLENATORI_CORSI` add constraint `FKcorsi`
     foreign key (`codiceCorso`)
     references `CORSI` (`codiceCorso`);

alter table `ARMADIETTI` add constraint `FKpossedere`
     foreign key (`idSpogliatoio`)
     references `SPOGLIATOI` (`idSpogliatoio`);

alter table `ARTICOLI_SPORTIVI` add constraint `FKappartiene`
     foreign key (`codiceTipologia`)
     references `TIPOLOGIE_PRODOTTI` (`codiceTipologia`);

alter table `CAMPI` add constraint `FKdisporre`
     foreign key (`idSpogliatoio`)
     references `SPOGLIATOI` (`idSpogliatoio`);

alter table `CORSI` add constraint `FKrelativo`
     foreign key (`codiceSport`)
     references `SPORT` (`codiceSport`);

alter table `INTERVENTI_PULIZIA` add constraint `FKpul_ADD`
     foreign key (`codiceFiscale_Addetto`)
     references `ADDETTI_PULIZIE` (`codiceFiscale`);

alter table `INTERVENTI_PULIZIA` add constraint `FKpul_SPO`
     foreign key (`idSpogliatoio`)
     references `SPOGLIATOI` (`idSpogliatoio`);

alter table `OCCUPAZIONI_CAMPI` add constraint `FKdurante`
     foreign key (`idFascia`)
     references `FASCE_ORARIE` (`idFascia`);

alter table `OCCUPAZIONI_CAMPI` add constraint `FKeffettua`
     foreign key (`codiceCorso`)
     references `CORSI` (`codiceCorso`);

alter table `OCCUPAZIONI_CAMPI` add constraint `FKaffittare`
     foreign key (`codiceFiscale_Iscritto`)
     references `ISCRITTI` (`codiceFiscale`);

alter table `OCCUPAZIONI_CAMPI` add constraint `FKluogo`
     foreign key (`codiceCampo`)
     references `CAMPI` (`codiceCampo`);

alter table `SEDUTE` add constraint `FKsed_ISC`
     foreign key (`codiceFiscale_Iscritto`)
     references `ISCRITTI` (`codiceFiscale`);

alter table `SEDUTE` add constraint `FKsed_FIS`
     foreign key (`codiceFiscale_Fisioterapista`)
     references `FISIOTERAPISTI` (`codiceFiscale`);

alter table `TESSERE` add constraint `FKricevere_FK`
     foreign key (`codiceFiscale_Iscritto`)
     references `ISCRITTI` (`codiceFiscale`);


#------------------------------INSERIMENTO DATI ---------------------------#
INSERT INTO ISCRITTI VALUES('CMPNDR99D20H294V','Andrea','Acampora','3334455654','1999-03-20','andrea@mail.com');
INSERT INTO ISCRITTI VALUES('BNCGHF98R23H321P','Giulio','Banco','3213422694','1999-08-15','luigi@mail.com');
INSERT INTO ISCRITTI VALUES('PLLNDR89D10H484J','Andrea','Pollini','3223759847','1999-09-17','pollini@mail.com');
INSERT INTO ISCRITTI VALUES('RSSMRC77F34J332P','Marco','Rossi','3332265654','1999-10-10','marco@mail.com');
INSERT INTO ISCRITTI VALUES('TLNVVT86S43H143F','Giulio','Banco','2791356428','1999-03-20','luigi@mail.com');
INSERT INTO ISCRITTI VALUES('DSZPJV28T05E662T','Noemi','Ferri','3327774400','1999-03-20','noemi@mail.com');
INSERT INTO ISCRITTI VALUES('QRJHJT52S14L758Y','Norberto','Moretti','3324368080','1999-03-20','norberto@mail.com');
INSERT INTO ISCRITTI VALUES('DCVGWL60D13A286M','Cupido','Russo','3203855765','1999-03-20','cupido@mail.com');
INSERT INTO ISCRITTI VALUES('WXLYBG63D09B033H','Aloisia','Pisani','3286726771','1999-03-20','aloisa@mail.com');
INSERT INTO ISCRITTI VALUES('BKXLWJ81L14H545S','Elia','Folliero','3336435513','1998-03-20','elia@mail.com');
INSERT INTO ISCRITTI VALUES('VGKZKD83E28D304P','Nereo','Palermo','3233422694','1998-03-20','nereo@mail.com');
INSERT INTO ISCRITTI VALUES('FZSHJL85M05B509X','Alvaro','Piccio','3667234743','1998-03-20','alvaro@mail.com');
INSERT INTO ISCRITTI VALUES('BHGRGO50L44C297B','Celio','Siciliano','3066744854','1998-03-20','celio@mail.com');
INSERT INTO ISCRITTI VALUES('FBMVFR46A51E758T','Alessia','Romano','3777788252','1998-03-20','alessia@mail.com');
INSERT INTO ISCRITTI VALUES('SJJCYV90L09F754P','Marco','Rizzo','3427260370','1998-03-20','marco@mail.com');
INSERT INTO ISCRITTI VALUES('PQNZVN92R23L087I','Cipriano','Nucci','3377202567','1998-03-20','cipriano@mail.com');
INSERT INTO ISCRITTI VALUES('SBHCRR66H30G459K','Lilla','Padovesi','3918850650','1998-03-20','lilla@mail.com');
INSERT INTO ISCRITTI VALUES('WRDZDG66M20I824I','Romola','Napolitano','3116547246','1997-03-20','romola@mail.com');
INSERT INTO ISCRITTI VALUES('FXGNGH58P44I791B','Azzurra','Giordano','3465530606','1997-03-20','azzurra@mail.com');
INSERT INTO ISCRITTI VALUES('BNCGHF98R23H329A','Remo','Iadanza','3892881571','1997-03-20','remo@mail.com');
INSERT INTO ISCRITTI VALUES('LTDNQL66T29F343L','Manuela','Fonti','3757818490','1997-03-20','manuela@mail.com');
INSERT INTO ISCRITTI VALUES('TCPYGZ74H06H270A','Quintino','Greco','3689854910','1997-03-20','quintino@mail.com');
INSERT INTO ISCRITTI VALUES('RPRBGR66M43A802I','Pancrazio','Russo','3820322608','1997-03-20','pancrazio@mail.com');
INSERT INTO ISCRITTI VALUES('LBMMZT67B15A239M','Leopoldo','Siciliani','3668719737','1997-03-20','leopoldo@mail.com');
INSERT INTO ISCRITTI VALUES('ZTSMHC73B53L147T','Generosa','Capon','3923358327','1997-03-20','generosa@mail.com');
INSERT INTO ISCRITTI VALUES('GHRFQZ53B27M272A','Danilo','Loggia','3757017839','1997-03-20','danilo@mail.com');
INSERT INTO ISCRITTI VALUES('XKNLFW35H55I609K','Saverio','Cremonesi','3731604456','1997-03-20','saverio@mail.com');
INSERT INTO ISCRITTI VALUES('FBDRDB33H64A956Z','Luca','Trentino','3839623573','1997-03-20','luca@mail.com');
INSERT INTO ISCRITTI VALUES('LPFMLB45R11E168X','Alessio','Padovano','3848401549','1997-03-20','alessio@mail.com');
INSERT INTO ISCRITTI VALUES('ZMWMCP75R26A023Q','Marco','Fallaci','3900258599','1997-03-20','marco@mail.com');


INSERT INTO TIPOLOGIE_ABBONAMENTI VALUES(12,500,100);
INSERT INTO TIPOLOGIE_ABBONAMENTI VALUES(9,400,80);
INSERT INTO TIPOLOGIE_ABBONAMENTI VALUES(6,300,60);
INSERT INTO TIPOLOGIE_ABBONAMENTI VALUES(3,200,30);


INSERT INTO SPORT VALUES('CAL','CALCIO');
INSERT INTO SPORT VALUES('PAL','PALLAVOLO');
INSERT INTO SPORT VALUES('BAS', 'BASKET');


INSERT INTO CORSI VALUES('CAL01','CAL','CALCIO 2O',20,30);
INSERT INTO CORSI VALUES('CAL02','CAL','CALCIO 18',18,30);
INSERT INTO CORSI VALUES('CAL03','CAL','CALCIO 16',16,30);
INSERT INTO CORSI VALUES('BAS01','BAS','BASKET 20',20,30);
INSERT INTO CORSI VALUES('BAS02','BAS','BASKET 18',18,30);
INSERT INTO CORSI VALUES('BAS03','BAS','BASKET 16',16,30);
INSERT INTO CORSI VALUES('PAL01','PAL','PALLAVOLO 20',20,30);
INSERT INTO CORSI VALUES('PAL02','PAL','PALLAVOLO 18',28,30);
INSERT INTO CORSI VALUES('PAL03','PAL','PALLAVOLO 16',16,30);


INSERT INTO ABBONAMENTI VALUES(12,01,'2019-01-10','2019-01-11','CMPNDR99D20H294V','CAL01');
INSERT INTO ABBONAMENTI VALUES(9,02,'2019-02-15','2019-02-20','BNCGHF98R23H321P','BAS01');
INSERT INTO ABBONAMENTI VALUES(12,03,'2019-04-9','2019-04-10','PLLNDR89D10H484J','PAL01');
INSERT INTO ABBONAMENTI VALUES(9,04,'2019-01-15','2019-01-20','RSSMRC77F34J332P','CAL02');
INSERT INTO ABBONAMENTI VALUES(12,05,'2019-01-15','2019-01-20','TLNVVT86S43H143F','CAL01');
INSERT INTO ABBONAMENTI VALUES(6,06,'2019-01-15','2019-01-20','DSZPJV28T05E662T','PAL02');
INSERT INTO ABBONAMENTI VALUES(12,07,'2019-01-15','2019-01-20','QRJHJT52S14L758Y','CAL01');
INSERT INTO ABBONAMENTI VALUES(9,08,'2019-01-15','2019-01-20','BKXLWJ81L14H545S','CAL01');
INSERT INTO ABBONAMENTI VALUES(6,09,'2019-01-15','2019-01-20','BNCGHF98R23H329A','PAL02');
INSERT INTO ABBONAMENTI VALUES(6,10,'2019-01-15','2019-01-20','LPFMLB45R11E168X','BAS03');


INSERT INTO TIPOLOGIE_PRODOTTI VALUES('N4A', 30, 'Pantaloncini Nike');
INSERT INTO TIPOLOGIE_PRODOTTI VALUES('A3B', 20, 'Pantaloncini Adidas');
INSERT INTO TIPOLOGIE_PRODOTTI VALUES('Q0Q', 50, 'Maglia Puma');
INSERT INTO TIPOLOGIE_PRODOTTI VALUES('A9A', 5, 'Calzettoni Nike');
INSERT INTO TIPOLOGIE_PRODOTTI VALUES('S2L', 20, 'Pantaloni lunghi Adidas');


INSERT INTO ARTICOLI_SPORTIVI VALUES ('N4A','N4A1','L');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('N4A','N4A2','M');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('N4A','N4A3','XL');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('N4A','N4A4','XXM');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('A3B','A3B1','S');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('A3B','A3B2','XS');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('A3B','A3B3','M');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('A3B','A3B4','L');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('Q0Q','Q0Q1','L');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('Q0Q','Q0Q2','M');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('A9A','A9A1','S');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('A9A','A9A2','XS');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('S2L','S2L1','M');
INSERT INTO ARTICOLI_SPORTIVI VALUES ('S2L','S2L2','XXS');


INSERT INTO ALLENATORI VALUES('CNTNTN69D20H294V','Antonio','Conte','331155654','antonio@mail.com',30,'A');
INSERT INTO ALLENATORI VALUES('GRDGSP93G76HFRB2','Giuseppe','Guardiola','3112498654','giuseppe@mail.com',25,'B');
INSERT INTO ALLENATORI VALUES('VHV735FGCKE836EG','Marco','Bollini','7654987567','marco@mail.com',30,'A');
INSERT INTO ALLENATORI VALUES('PDU76FVCBGET3JHF','Luca','Ventura','7698546754','luca@mail.com',30,'A');
INSERT INTO ALLENATORI VALUES('VFJVNWU2475HRBCR','Massimo','Allegri','9867564876','massimo@mail.com',20,'C');
INSERT INTO ALLENATORI VALUES('YHV7EYCBUW64736F','Pietro','Bolli','9087674537','pietro@mail.com',10,'D');
INSERT INTO ALLENATORI VALUES('VBHEWBHFRBWUFJ31','Enzo','Pertone','3456532134','enzo@mail.com',30,'A');
INSERT INTO ALLENATORI VALUES('ADOJCN82942CDWH3','Remo','Vittore','3234543784','remo@mail.com',15,'D');
INSERT INTO ALLENATORI VALUES('VFWIRV88834BFRHW','Alfio','Tranpolo','3876343221','alfio@mail.com',20,'C');


INSERT INTO ADDETTI_PULIZIE VALUES('CNTNTN45D20H294V','Matteo','Contini','331155654','matteo@mail.com',30);
INSERT INTO ADDETTI_PULIZIE VALUES('HF64FDT3G76HFRB2','Giovanni','Gallini','3112498654','giuseppe@mail.com',25);
INSERT INTO ADDETTI_PULIZIE VALUES('V6HBG35FGCKE836E','Achille','Boni','7654987567','achi@mail.com',30);
INSERT INTO ADDETTI_PULIZIE VALUES('ER6PL9FVCBGET3JH','Nicholas','Fura','7698546754','nico@mail.com',30);
INSERT INTO ADDETTI_PULIZIE VALUES('BVF4DWU2475HRBCR','Filippo','Gegri','9867564876','filippo@mail.com',20);
INSERT INTO ADDETTI_PULIZIE VALUES('CDX43YCBUW64736F','Michele','Belli','9087674537','michele@mail.com',10);
INSERT INTO ADDETTI_PULIZIE VALUES('PL9IU6BHFRBWUFJ3','Andrea','Perrone','3456532134','andrea@mail.com',30);


INSERT INTO FISIOTERAPISTI VALUES('QWBLZP29A19Z309L','Mario','Rossi','3334455334','mario@mail.com', 20, 'traumi muscolari', 1991);
INSERT INTO FISIOTERAPISTI VALUES('PFJVXC57L06A544M','Ada', 'Romani','3234455334','ada@mail.com',12, 'distorsioni', 2000);
INSERT INTO FISIOTERAPISTI VALUES('RRBPGB73D43B762T','Arnaldo','Marcelo','3344455334','arnaldo@mail.com',13, 'traumi muscolari', 2002);
INSERT INTO FISIOTERAPISTI VALUES('RJQPLI92C55M050L','Giorgia','Genovese','3334455331','giorgia@mail.com',21, 'distorsioni', 2003);
INSERT INTO FISIOTERAPISTI VALUES('HRFDHM53M45G471W','Matteo','Pugliesi','3339455334','matteo@mail.com', 19, 'radiologia', 2001);
INSERT INTO FISIOTERAPISTI VALUES('YRTMSS66A48B280B','Vala','Arcuri','3339455434','vala@mail.com',10, 'traumi muscolari', 1999);
INSERT INTO FISIOTERAPISTI VALUES('HFGHNB96L13M162X','Rosina','Bucco','3332355334','rosina@mail.com', 14, 'distorsioni', 1998);
INSERT INTO FISIOTERAPISTI VALUES('DTRZPM84E51I177N','Gaetano','Bruno','3339455354','gaetano@mail.com', 30, 'radiologia', 1997);


INSERT INTO SPOGLIATOI VALUES('1AS', 100, 25);
INSERT INTO SPOGLIATOI VALUES('2LA', 200, 30);
INSERT INTO SPOGLIATOI VALUES('23S', 120, 21);
INSERT INTO SPOGLIATOI VALUES('L4A', 102, 12);
INSERT INTO SPOGLIATOI VALUES('S5D', 110, 23);
INSERT INTO SPOGLIATOI VALUES('W6C', 150, 21);


INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('N4A','N4A1','2020-06-08',15,'CMPNDR99D20H294V');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('N4A','N4A2','2020-06-07',25,'BNCGHF98R23H321P');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('A3B','A3B1','2020-06-09',45,'PLLNDR89D10H484J');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('A3B','A3B2','2020-06-02',30,'RSSMRC77F34J332P');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('Q0Q','Q0Q1','2020-05-09',15,'RPRBGR66M43A802I');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('Q0Q','Q0Q2','2020-02-04',15,'LPFMLB45R11E168X');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('A9A','A9A1','2020-03-10',60,'SBHCRR66H30G459K');
INSERT INTO ACQUISTI (codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) VALUES ('S2L','S2L1','2020-05-28',75,'FXGNGH58P44I791B');


INSERT INTO TESSERE (codiceFiscale_Iscritto, punti) VALUES('CMPNDR99D20H294V',100);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti) VALUES('BNCGHF98R23H321P',600);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti) VALUES('PLLNDR89D10H484J',300);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti) VALUES('RSSMRC77F34J332P',450);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('TLNVVT86S43H143F',550);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('DSZPJV28T05E662T',780);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('QRJHJT52S14L758Y',500);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('DCVGWL60D13A286M',300);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('WXLYBG63D09B033H',200);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('BKXLWJ81L14H545S',1300);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('VGKZKD83E28D304P',9000);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('FZSHJL85M05B509X',330);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('BHGRGO50L44C297B',700);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('FBMVFR46A51E758T',900);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('SJJCYV90L09F754P',50);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('PQNZVN92R23L087I',430);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('SBHCRR66H30G459K',320);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('WRDZDG66M20I824I',170);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('FXGNGH58P44I791B',670);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('BNCGHF98R23H329A',540);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('LTDNQL66T29F343L',270);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('TCPYGZ74H06H270A',870);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('RPRBGR66M43A802I',480);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('LBMMZT67B15A239M',590);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('ZTSMHC73B53L147T',370);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('GHRFQZ53B27M272A',920);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('XKNLFW35H55I609K',160);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('FBDRDB33H64A956Z',530);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('LPFMLB45R11E168X',280);
INSERT INTO TESSERE (codiceFiscale_Iscritto, punti)  VALUES('ZMWMCP75R26A023Q',870);


