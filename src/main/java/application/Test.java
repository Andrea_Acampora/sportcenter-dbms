package application;

import java.sql.Date;

import db.Iscritti_Table;
import db.model.Iscritto;

public class Test {
    
        @SuppressWarnings("deprecation")
        public static void main(String[] args)  {
        
        Iscritti_Table iscrittiTable = new Iscritti_Table();
        
        Iscritto iscritto = new Iscritto();
        iscritto.setCodiceFiscale("CMPNDR99C20H294V");
        iscritto.setNome("Andrea");
        iscritto.setCognome("Acampora");
        iscritto.setTelefono("3348459754");
        iscritto.setMail("Andrea@mail.com");
        iscritto.setDataNascita(new java.util.Date(1999,03,20));
        
        iscrittiTable.persist(iscritto);
        
        System.out.println(iscrittiTable.findByPrimaryKey("CMPNDR99C20H294V").getNome());
    }
}
