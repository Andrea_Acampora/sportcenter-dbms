package controllers;

import db.TipologieProdottiTable;
import db.model.TipologiaProdotto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class TipologiaProdottiController {

    
    @FXML
    private TextField codice; 
    
    @FXML
    private TextField prezzo; 
    
    @FXML
    private TextField descrizione;
    
    @FXML
    private void handleButtonAction(ActionEvent event)
    {
        TipologiaProdotto prodotto = new TipologiaProdotto();
        prodotto.setCodiceTipologia(codice.getText());
        prodotto.setPrezzo(Integer.parseInt(prezzo.getText()));
        prodotto.setDescrizione(descrizione.getText());
        TipologieProdottiTable tipologia = new TipologieProdottiTable(); 
        tipologia.persist(prodotto);
    }
}
