package controllers;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import db.Allenatori_Table;
import db.Iscritti_Table;
import db.model.Allenatore;
import db.model.Iscritto;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class AllenatoreController implements Initializable{
    
    @FXML
    private TableView<Allenatore> table;
    
    @FXML
    private TableColumn<Allenatore, String> codice;
    
    @FXML
    private TableColumn<Allenatore, String> nome;
    
    @FXML
    private TableColumn<Allenatore, String> cognome;
    
    @FXML
    private TableColumn<Allenatore, String> telefono;
    
    @FXML
    private TableColumn<Allenatore, String> mail;
  
    @FXML
    private TableColumn<Allenatore, Integer> paga;
    
    @FXML
    private TableColumn<Allenatore, String> grado;
    
    @FXML
    private TextField Codice; 
    
    @FXML
    private TextField Nome; 
    
    @FXML
    private TextField Cognome; 
    
    @FXML
    private TextField Telefono; 
    
    @FXML
    private TextField Mail; 
    
    @FXML
    private TextField Paga; 

    @FXML
    private TextField Grado; 
    
    @FXML
    private void handleButtonAction(ActionEvent event)
    {
    
        Allenatore allenatore = new Allenatore(); 
        allenatore.setCodiceFiscale(Codice.getText());
        allenatore.setNome(Nome.getText());
        allenatore.setCognome(Cognome.getText());
        allenatore.setTelefono(Telefono.getText());
        allenatore.setMail(Mail.getText());
        allenatore.setPagaOraria(Integer.parseInt(Paga.getText()));
        allenatore.setGradoPatentino(Grado.getText());
        Allenatori_Table allTable = new Allenatori_Table(); 
        allTable.persist(allenatore);
    }

    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.codice.setCellValueFactory(cellData -> cellData.getValue().codiceFiscaleProperty());
        this.nome.setCellValueFactory(cellData -> cellData.getValue().nomeProperty());
        this.cognome.setCellValueFactory(cellData -> cellData.getValue().cognomeProperty());
        this.telefono.setCellValueFactory(cellData -> cellData.getValue().telefonoProperty());
        this.mail.setCellValueFactory(cellData -> cellData.getValue().mailProperty());
        this.paga.setCellValueFactory(cellData -> cellData.getValue().pagaOrariaProperty()); 
        this.grado.setCellValueFactory(cellData -> cellData.getValue().gradoPatentinoProperty()); 
        this.table.setItems(this.getAllenatori());        
    }
    
    private ObservableList<Allenatore> getAllenatori() {
        Allenatori_Table db_table = new Allenatori_Table();
        ObservableList<Allenatore> allenatori = db_table.getData();
        return allenatori;
    }
    
    
}
