package controllers;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import db.Abbonamenti_Table;
import db.Iscritti_Table;
import db.model.Abbonamento;
import db.model.Iscritto;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;


public class AbbonamentiController implements Initializable{
    
    @FXML
    private TableView<Abbonamento> table;
    
    @FXML
    private TableColumn<Abbonamento, Integer> codice;
    
    @FXML
    private TableColumn<Abbonamento, Integer> durata;
    
    @FXML
    private TableColumn<Abbonamento, String> iscritto;
    
    @FXML
    private TableColumn<Abbonamento, String> corso;
    
    @FXML
    private TableColumn<Abbonamento, Date> data;
    
    @FXML
    private TableColumn<Abbonamento, Date> inizio;
    
    @FXML
    private TextField codCorso; 
    
    @FXML
    private TextField codIscritto; 
    
    @FXML
    private TextField dur; 
    
    @FXML
    private TextField dataSott; 
    
    @FXML
    private TextField dataInizio; 

    
    @FXML
    private void handleButton(ActionEvent event)
    {
    
        Abbonamento abbo = new Abbonamento(); 
        abbo.setCodiceFiscale_Iscritto(codIscritto.getText());
        abbo.setCodiceCorso(codCorso.getText());
        abbo.setDurata(Integer.parseInt(dur.getText()));
        abbo.setDataSottoscrizione(new Date(dataSott.getText().codePointBefore(10)));
        abbo.setInizioValidita(new Date(dataSott.getText().codePointBefore(10)));
        Abbonamenti_Table abbo_table = new Abbonamenti_Table(); 
        abbo_table.persist(abbo);
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.codice.setCellValueFactory(cellData -> cellData.getValue().codiceAbbonamentoProperty());
        this.durata.setCellValueFactory(cellData -> cellData.getValue().durataProperty());
        this.corso.setCellValueFactory(cellData -> cellData.getValue().codiceCorsoProperty());
        this.iscritto.setCellValueFactory(cellData -> cellData.getValue().codiceFiscale_IscrittoProperty());
        this.data.setCellValueFactory(new PropertyValueFactory<Abbonamento, Date>("dataSottoscrizione"));  
        this.inizio.setCellValueFactory(new PropertyValueFactory<Abbonamento, Date>("inizioValidita"));  
        this.table.setItems(this.getAbbonamenti());        
    }
    
    public ObservableList<Abbonamento> getAbbonamenti() {
        Abbonamenti_Table db_table = new Abbonamenti_Table();
        ObservableList<Abbonamento> abbonati = db_table.getData();
        return abbonati;
    }
    
}
