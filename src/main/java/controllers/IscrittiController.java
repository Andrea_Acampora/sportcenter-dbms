package controllers;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import db.Iscritti_Table;
import db.model.Iscritto;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;


public class IscrittiController implements Initializable{
    @FXML
    private TableView<Iscritto> table;
    
    @FXML
    private TableColumn<Iscritto, String> codice;
    
    @FXML
    private TableColumn<Iscritto, String> nome;
    
    @FXML
    private TableColumn<Iscritto, String> cognome;
    
    @FXML
    private TableColumn<Iscritto, String> telefono;
    
    @FXML
    private TableColumn<Iscritto, String> mail;
    
    @FXML
    private TableColumn<Iscritto, Date> nascita;
    
    @FXML
    private TextField Codice; 
    
    @FXML
    private TextField Nome; 
    
    @FXML
    private TextField Cognome; 
    
    @FXML
    private TextField Telefono; 
    
    @FXML
    private TextField Mail; 
    
    @FXML
    private TextField Giorno; 

    @FXML
    private TextField Mese; 

    @FXML
    private TextField Anno; 

    @FXML
    private void handleButton(ActionEvent event)
    {
    
        Iscritto iscritto = new Iscritto(); 
        iscritto.setCodiceFiscale(Codice.getText());
        iscritto.setNome(Nome.getText());
        iscritto.setCognome(Cognome.getText());
        iscritto.setTelefono(Telefono.getText());
        iscritto.setMail(Mail.getText());
        iscritto.setDataNascita(new java.util.Date(Integer.parseInt(Anno.getText(),Integer.parseInt(Mese.getText(), Integer.parseInt(Giorno.getText())))));
        Iscritti_Table iscTable = new Iscritti_Table(); 
        iscTable.persist(iscritto);
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.codice.setCellValueFactory(cellData -> cellData.getValue().codiceFiscaleProperty());
        this.nome.setCellValueFactory(cellData -> cellData.getValue().nomeProperty());
        this.cognome.setCellValueFactory(cellData -> cellData.getValue().cognomeProperty());
        this.telefono.setCellValueFactory(cellData -> cellData.getValue().telefonoProperty());
        this.mail.setCellValueFactory(cellData -> cellData.getValue().mailProperty());
        this.nascita.setCellValueFactory(new PropertyValueFactory<Iscritto, Date>("dataNascita"));  
        this.table.setItems(this.getIscritti());        
    }
    
    private ObservableList<Iscritto> getIscritti() {
        Iscritti_Table db_table = new Iscritti_Table();
        ObservableList<Iscritto> iscritti = db_table.getData();
        return iscritti;
    }
    
}
