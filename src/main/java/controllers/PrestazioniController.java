package controllers;

import db.InterventiPuliziaTable;
import db.SeduteTable;
import db.TipologieProdottiTable;
import db.model.InterventoPulizia;
import db.model.Seduta;
import db.model.TipologiaProdotto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class PrestazioniController {

    @FXML
    private TextField codiceFF;
    
    @FXML
    private TextField esitoF;
    
    @FXML
    private TextField codiceIF; 
    
    @FXML
    private TextField durataF; 
    
    @FXML
    private TextField giornoF; 
    
    @FXML
    private TextField meseF; 
    
    @FXML
    private TextField annoF; 
    
    @FXML
    private TextField spogliatoio; 
    
    @FXML
    private TextField codiceS; 
    
    @FXML
    private TextField durataS; 
    
    @FXML
    private TextField giornoS; 
    
    @FXML
    private TextField meseS; 
    
    @FXML
    private TextField annoS; 
    
    
    @FXML
    private void insertSeduta(ActionEvent event)
    {
        Seduta seduta = new Seduta(); 
        seduta.setCodiceFiscale_Fisioterapista(codiceFF.getText());
        seduta.setCodiceFiscale_Iscritto(codiceIF.getText());
        seduta.setDurata(Integer.parseInt(durataF.getText()));
        seduta.setEsito(esitoF.getText());
        seduta.setData(new java.util.Date(Integer.parseInt(annoF.getText(),Integer.parseInt(meseF.getText(), Integer.parseInt(giornoF.getText())))));
        SeduteTable sedTable = new SeduteTable(); 
        sedTable.persist(seduta);
        
    }
    
    
    
    @FXML
    private void insertPulizia(ActionEvent event)
    {
        InterventoPulizia pulizia = new InterventoPulizia(); 
        pulizia.setIdSpogliatoio(spogliatoio.getText());
        pulizia.setCodiceFiscale_Addetto(codiceS.getText());
        pulizia.setDurata(Integer.parseInt(durataS.getText()));
        pulizia.setData(new java.util.Date(Integer.parseInt(annoS.getText(),Integer.parseInt(meseS.getText(), Integer.parseInt(giornoS.getText())))));
        InterventiPuliziaTable pulTable = new InterventiPuliziaTable(); 
        pulTable.persist(pulizia);
    }
}
