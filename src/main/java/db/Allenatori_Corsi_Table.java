package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Allenatori_Corsi;

public class Allenatori_Corsi_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Allenatori_Corsi_Table() {
        dataSource = new DBConnection();
        tableName="ALLENATORI_CORSI";
    }
  
    public void persist(Allenatori_Corsi allenatore_corso) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(allenatore_corso.getCodiceFiscale_Allenatore(), allenatore_corso.getCodiceCorso())!=null){
             new Exception("Allenatore_Corso exists");
             System.out.println("Errore"+ "allenatore_corso  exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceFiscale_Allenatore, codiceCorso) values (?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, allenatore_corso.getCodiceFiscale_Allenatore());
            statement.setString(2, allenatore_corso.getCodiceCorso());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Allenatori_Corsi findByPrimaryKey(String codiceFiscale_Allenatore, String codiceCorso)  {
        Allenatori_Corsi allenatore_corso = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceFiscale_Allenatore=? AND codiceCorso = ?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceFiscale_Allenatore);
            statement.setString(2, codiceCorso);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                allenatore_corso = new Allenatori_Corsi();
                allenatore_corso.setCodiceFiscale_Allenatore(result.getString("codiceFiscale_Allenatore"));
                allenatore_corso.setCodiceCorso(result.getString("codiceCorso"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return allenatore_corso;
    } 
}
