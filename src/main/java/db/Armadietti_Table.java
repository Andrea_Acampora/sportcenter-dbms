package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Armadietto;

public class Armadietti_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Armadietti_Table() {
        dataSource = new DBConnection();
        tableName="ARMADIETTI";
    }
  
    public void persist(Armadietto armadietto) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(armadietto.getIdSpogliatoio(), armadietto.getNumero())!=null){
             new Exception("armadietto exists");
             System.out.println("Errore"+ "armadietto  exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (idSpogliatoio, numero) values (?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, armadietto.getIdSpogliatoio());
            statement.setInt(2, armadietto.getNumero() );
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Armadietto findByPrimaryKey(String idSpogliatoio, int numero)  {
        Armadietto armadietto = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where idSpogliatoio=? AND numero =?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, idSpogliatoio);
            statement.setInt(2, numero);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                armadietto = new Armadietto();
                armadietto.setIdSpogliatoio(result.getString("idSpogliatoio"));
                armadietto.setNumero(result.getInt("numero"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return armadietto;
    } 
}
