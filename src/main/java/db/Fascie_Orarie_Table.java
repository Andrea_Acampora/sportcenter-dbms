package db;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;

import db.model.Fascia_Oraria;

class Fascie_Orarie_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Fascie_Orarie_Table() {
        dataSource = new DBConnection();
        tableName="FASCE_ORARIE";
    }
  
    public void persist(Fascia_Oraria fascia) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(fascia.getIdFascia())!=null){
             new Exception("armadietto exists");
             System.out.println("Errore"+ "armadietto  exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (idFascia, ora_inizio, ora_fine) values (?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, fascia.getIdFascia());
            statement.setInt(2, fascia.getOra_inizio());
            statement.setInt(3, fascia.getOra_fine());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Fascia_Oraria findByPrimaryKey(String idFascia)  {
        Fascia_Oraria fascia = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where idFascia=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, idFascia);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                fascia = new Fascia_Oraria();
                fascia.setIdFascia(result.getString("idFascia"));
                fascia.setOra_fine(result.getInt("ora_fine"));
                fascia.setOra_inizio(result.getInt("ora_inizio"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return fascia;
    } 
}
