package db;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.InterventoPulizia;
import db.model.Seduta;
import db.model.TipologiaAbbonamento;

public class InterventiPuliziaTable {
    private DBConnection dataSource;
    private String tableName;
    
    public InterventiPuliziaTable() {
        dataSource = new DBConnection();
        tableName="INTERVENTI_PULIZIA";
    }
  
    public void persist(InterventoPulizia interventoPulizia) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(interventoPulizia.getIdSpogliatoio(), interventoPulizia.getIdSpogliatoio(), interventoPulizia.getData())!=null){
             new Exception("intervento pulizia exists");
             System.out.println("Errore"+ "intervento pulizia exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (idSpogliatoio, codiceFiscale_Addetto, durata, data) values (?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, interventoPulizia.getIdSpogliatoio()); 
            statement.setString(2, interventoPulizia.getCodiceFiscale_Addetto());
            statement.setInt(3, interventoPulizia.getDurata());
            statement.setDate(4, new java.sql.Date(interventoPulizia.getData().getTime()));
            
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public InterventoPulizia findByPrimaryKey(String idSpogliatoio, String codiceFiscale_Addetto, Date data)  {
        InterventoPulizia interventoPulizia = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where idSpogliatoio=? and codiceFiscale_Addetto=? and data=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, idSpogliatoio);
            statement.setString(2, codiceFiscale_Addetto);
            statement.setDate(3, new java.sql.Date(data.getTime()));
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                interventoPulizia = new InterventoPulizia();                                
                interventoPulizia.setIdSpogliatoio(result.getString("idSpogliatoio"));
                interventoPulizia.setCodiceFiscale_Addetto(result.getString("codiceFiscale_Addetto"));
                interventoPulizia.setDurata(result.getInt("durata"));
                interventoPulizia.setData(result.getDate("data"));
                
                
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return interventoPulizia;
    } 
}
