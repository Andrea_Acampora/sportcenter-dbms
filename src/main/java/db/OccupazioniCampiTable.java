package db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import db.connection.DBConnection;
import db.model.OccupazioneCampo;

public class OccupazioniCampiTable {
    private DBConnection dataSource;
    private String tableName;
    
    public OccupazioniCampiTable() {
        dataSource = new DBConnection();
        tableName="OCCUPAZIONI_CAMPI";
    }
  
    public void persist(OccupazioneCampo occupazioneCampo) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(occupazioneCampo.getCodiceCampo(), occupazioneCampo.getIdFascia(), occupazioneCampo.getData())!=null){
             new Exception("occupazione campo exists");
             System.out.println("Errore"+ "occupazione campo exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceCampo, idFascia, data, codiceCorso, codiceFiscale_Iscritto) values (?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, occupazioneCampo.getCodiceCampo()); 
            statement.setString(2, occupazioneCampo.getIdFascia());
            statement.setDate(3, occupazioneCampo.getData());
            statement.setString(4, occupazioneCampo.getCodiceCorso());
            statement.setString(5, occupazioneCampo.getCodiceFiscale_Iscritto());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public OccupazioneCampo findByPrimaryKey(String codiceCampo, String idFascia, Date data)  {
        OccupazioneCampo occupazioneCampo = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceCampo=? and idFascia=? and data=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceCampo);
            statement.setString(2, idFascia);
            statement.setDate(3, data);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                occupazioneCampo = new OccupazioneCampo();                                                
                occupazioneCampo.setCodiceCampo(result.getString("codiceCampo"));
                occupazioneCampo.setIdFascia(result.getString("idFascia"));
                occupazioneCampo.setData(result.getDate("data"));
                occupazioneCampo.setCodiceCorso(result.getString("codiceCorso"));
                occupazioneCampo.setCodiceFiscale_Iscritto(result.getString("codiceFiscale_Iscritto"));
            }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return occupazioneCampo;
    } 
}
