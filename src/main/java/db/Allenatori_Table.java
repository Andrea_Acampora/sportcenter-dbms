package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Allenatore;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Allenatori_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Allenatori_Table() {
        dataSource = new DBConnection();
        tableName="ALLENATORI";
    }
    
    public ObservableList<Allenatore> getData(){      
        ObservableList<Allenatore> list = FXCollections.observableArrayList();
        Connection connection = this.dataSource.getMySQLConnection();
        System.out.println(connection);
        PreparedStatement statement = null;
        String query = "select * from "+ tableName;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Allenatore allenatore = new Allenatore(); 
                allenatore.setCodiceFiscale(result.getString("codiceFiscale"));
                allenatore.setNome(result.getString("nome"));
                allenatore.setCognome(result.getString("cognome"));
                allenatore.setTelefono(result.getString("telefono"));
                allenatore.setMail(result.getString("mail"));
                allenatore.setPagaOraria(result.getInt("pagaOraria"));
                allenatore.setGradoPatentino(result.getString("gradoPatentino"));
                list.add(allenatore); 
                
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return list;
    } 
  
    public void persist(Allenatore allenatore) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(allenatore.getCodiceFiscale())!=null){
             new Exception("Allenatore exists");
             System.out.println("Errore"+ "allenatore  exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceFiscale, nome, cognome, telefono, mail, pagaOraria, gradoPatentino) values (?,?,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, allenatore.getCodiceFiscale());
            statement.setString(2, allenatore.getNome());
            statement.setString(3, allenatore.getCognome());
            statement.setString(4, allenatore.getTelefono());
            statement.setString(5, allenatore.getMail());
            statement.setInt(6, allenatore.getPagaOraria());
            statement.setString(7, allenatore.getGradoPatentino());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Allenatore findByPrimaryKey(String codiceFiscale)  {
        Allenatore allenatore = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceFiscale=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceFiscale);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                allenatore = new Allenatore();
                allenatore.setCodiceFiscale(result.getString("codiceFiscale"));
                allenatore.setNome(result.getString("nome"));
                allenatore.setCognome(result.getString("cognome"));
                allenatore.setTelefono(result.getString("telefono"));
                allenatore.setMail(result.getString("mail"));
                allenatore.setPagaOraria(result.getInt("pagaOraria"));
                allenatore.setGradoPatentino(result.getString("gradoPatentino"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return allenatore;
    } 
}
