package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.connection.DBConnection;
import db.model.Sport;

public class SportTable {
    private DBConnection dataSource;
    private String tableName;
    
    public SportTable() {
        dataSource = new DBConnection();
        tableName="SPORT";
    }
    
    public void dropAndCreateTable() {
        Connection connection = this.dataSource.getMySQLConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try{
                statement.executeUpdate("DROP TABLE " + tableName);
            }
            catch (SQLException e) {
            }
            statement.executeUpdate (
                "create table `SPORT` (\n" + 
                "     `codiceSport` varchar(5) not null,\n" + 
                "     `nome` varchar(20) not null,\n" + 
                "     constraint `IDSPORT` primary key (`codiceSport`));"
            );
            statement.close ();
        }
        catch (SQLException e) {
             new Exception(e.getMessage());
             System.out.println("Errore : "+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                 new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
    }
  
    public void persist(Sport sport) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(sport.getCodiceSport())!=null){
             new Exception("Sport exists");
             System.out.println("Errore"+ "Sport exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceSport, nome) values (?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, sport.getCodiceSport()); 
            statement.setString(2, sport.getNome());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Sport findByPrimaryKey(String codiceSport)  {
        Sport sport = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where idSpogliatoio=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceSport);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                sport = new Sport();                
                sport.setCodiceSport(result.getString("codiceSport"));
                sport.setNome(result.getString("nome"));
                
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return sport;
    } 
}
