package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.TipologiaAbbonamento;

public class TipologiaAbbonamentoTable {
    private DBConnection dataSource;
    private String tableName;
    
    public TipologiaAbbonamentoTable() {
        dataSource = new DBConnection();
        tableName="TIPOLOGIE_ABBONAMENTI";
    }
  
    public void persist(TipologiaAbbonamento tipologiaAbbonamento) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(tipologiaAbbonamento.getDurata())!=null){
             new Exception("tipologie abbonamenti exists");
             System.out.println("Errore"+ "tipologie abbonamenti exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (durata, prezzo, puntiOttenuti) values (?,?, ?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1, tipologiaAbbonamento.getDurata()); 
            statement.setInt(2, tipologiaAbbonamento.getPrezzo());
            statement.setInt(3, tipologiaAbbonamento.getPuntiOttenuti());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public TipologiaAbbonamento findByPrimaryKey(int durata)  {
        TipologiaAbbonamento tipologiaAbbonamento = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where durata=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, durata);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                tipologiaAbbonamento = new TipologiaAbbonamento();                                
                tipologiaAbbonamento.setDurata(result.getInt("durata"));
                tipologiaAbbonamento.setPrezzo(result.getInt("prezzo"));
                tipologiaAbbonamento.setPuntiOttenuti(result.getInt("puntiOttenuti"));
                
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return tipologiaAbbonamento;
    } 
}
