package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Campo;

public class Campi_Table {

    private DBConnection dataSource;
    private String tableName;
    
    public Campi_Table() {
        dataSource = new DBConnection();
        tableName="CAMPI";
    }
  
    public void persist(Campo campo) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(campo.getCodiceCampo())!=null){
             new Exception("Campo exists");
             System.out.println("Errore"+ "campo exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceCampo, superficie, costoOrario, tipologia_campo, idSpogliatoio) values (?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, campo.getCodiceCampo());
            statement.setInt(2, campo.getSuperficie());
            statement.setInt(3, campo.getCostoOrario());
            statement.setString(4, campo.getTipologiaCampo());
            statement.setString(5, campo.getIdSpogliatoio());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Campo findByPrimaryKey(String codiceCampo)  {
        Campo campo = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceCampo=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceCampo);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                campo = new Campo();
                campo.setCodiceCampo(result.getString("codiceCampo"));
                campo.setSuperficie(result.getInt("superficie"));
                campo.setCostoOrario(result.getInt("costoOrario"));
                campo.setTipologiaCampo(result.getString("tipologia_campo"));
                campo.setIdSpogliatoio(result.getString("idSpogliatoio"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return campo;
    } 
}
