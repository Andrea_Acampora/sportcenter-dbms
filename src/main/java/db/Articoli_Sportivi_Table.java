package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Articolo_Sportivo;


public class Articoli_Sportivi_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Articoli_Sportivi_Table() {
        dataSource = new DBConnection();
        tableName="ARTICOLI_SPORTIVI";
    }
  
    public void persist(Articolo_Sportivo articolo) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(articolo.getCodiceTipologia(), articolo.getCodiceArticolo())!=null){
             new Exception("articolo exists");
             System.out.println("Errore"+ "articolo  exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceTipologia, codice_articolo, taglia) values (?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, articolo.getCodiceTipologia());
            statement.setString(2, articolo.getCodiceArticolo());
            statement.setString(1, articolo.getTaglia());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Articolo_Sportivo findByPrimaryKey(String codiceTipologia, String codiceArticolo)  {
        Articolo_Sportivo articolo = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceTipologia=? AND codice_articolo=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceTipologia);
            statement.setString(2, codiceArticolo);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                articolo = new Articolo_Sportivo();
                articolo.setCodiceTipologia(result.getString("codiceTipologia"));
                articolo.setCodiceArticolo(result.getString("codice_articolo"));
                articolo.setTaglia(result.getString("taglia"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return articolo;
    } 
}
