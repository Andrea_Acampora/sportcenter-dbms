package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import db.connection.DBConnection;
import db.model.Tessera;

public class TessereTable {
    private DBConnection dataSource;
    private String tableName;
    
    public TessereTable() {
        dataSource = new DBConnection();
        tableName="TESSERE";
    }
  
    public void persist(Tessera tessera) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(tessera.getNumTessera())!=null){
             new Exception("tessera exists");
             System.out.println("Errore"+ "tessera exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (numTessera, codiceFiscale_Iscritto, punti) values (?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1, tessera.getNumTessera()); 
            statement.setString(2, tessera.getCodiceFiscale_Iscritto());
            statement.setInt(3, tessera.getPunti());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Tessera findByPrimaryKey(int numTessera)  {
        Tessera tessera = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where numTessera=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, numTessera);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                tessera = new Tessera();                                
                tessera.setNumTessera(result.getInt("numTessera"));
                tessera.setCodiceFiscale_Iscritto(result.getString("codiceFiscale_Iscritto"));
                tessera.setPunti(result.getInt("punti"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return tessera;
    } 
}
