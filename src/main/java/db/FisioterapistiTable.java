package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import db.connection.DBConnection;
import db.model.Fisioterapista;

public class FisioterapistiTable {
    private DBConnection dataSource;
    private String tableName;
    
    public FisioterapistiTable() {
        dataSource = new DBConnection();
        tableName = "FISIOTERAPISTI";
    }
    
    public void dropAndCreateTable() {
        Connection connection = this.dataSource.getMySQLConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try{
                statement.executeUpdate("DROP TABLE " + tableName);
            }
            catch (SQLException e) {
            }
            statement.executeUpdate (
                    "create table `FISIOTERAPISTI`("
                    + "`codiceFiscale` VARCHAR(16) not null,"
                    + "`nome` VARCHAR(20) not null,"
                    + "`cognome` VARCHAR(20) not null,"
                    + "`telefono` VARCHAR(15) not null,"
                    + "`mail` VARCHAR(30) not null,"
                    + "`pagaOraria` INTEGER not null,"
                    + "`specializzazione` VARCHAR(20) not null,"
                    + "`annoAbilitazione` INTEGER not null,"
                    + "constraint `IDFISIOTERAPISTA` primary key (`codiceFiscale`)) "
            ); 
            statement.close ();
        }
        catch (SQLException e) {
             new Exception(e.getMessage());
             System.out.println("Errore : "+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                 new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
    }
  
    public void persist(Fisioterapista fisioterapista) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(fisioterapista.getCodiceFiscale())!=null){
             new Exception("Fisioterapista exists");
             System.out.println("Errore"+ "Fsioterapista exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceFiscale, nome, cognome, telefono, mail, pagaOraria, specializzazione, annoAbilitazione) values (?,?,?,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, fisioterapista.getCodiceFiscale());
            statement.setString(2, fisioterapista.getNome());
            statement.setString(3, fisioterapista.getCognome());
            statement.setString(4, fisioterapista.getTelefono());
            statement.setString(5, fisioterapista.getMail());
            statement.setInt(5, fisioterapista.getPagaOraria());
            statement.setString(5, fisioterapista.getSpecializzazione());
            statement.setInt(5, fisioterapista.getAnnoAbilitazione());
            
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Fisioterapista findByPrimaryKey(String codiceFiscale)  {
        Fisioterapista fisioterapista = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceFiscale=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceFiscale);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                fisioterapista = new Fisioterapista();
                fisioterapista.setCodiceFiscale(result.getString("codiceFiscale"));
                fisioterapista.setNome(result.getString("nome"));
                fisioterapista.setCognome(result.getString("cognome"));
                fisioterapista.setTelefono(result.getString("telefono"));
                fisioterapista.setMail(result.getString("mail"));
                fisioterapista.setPagaOraria(result.getInt("pagaOraria"));
                fisioterapista.setSpecializzazione(result.getString("specializzazione"));
                fisioterapista.setAnnoAbilitazione(result.getInt("annoAbilitazione"));                
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return fisioterapista;
    } 
}

