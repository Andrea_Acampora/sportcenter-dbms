package db;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Seduta;

public class SeduteTable {
    private DBConnection dataSource;
    private String tableName;

    public SeduteTable() {
        dataSource = new DBConnection();
        tableName = "SEDUTE";
    }

    public void persist(Seduta seduta) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(seduta.getCodiceFiscale_Fisioterapista(), seduta.getCodiceFiscale_Iscritto(), seduta.getData()) != null) {
            new Exception("seduta exists");
            System.out.println("Errore" + "seduta exists");
        }

        PreparedStatement statement = null;
        String insert = "insert into " + tableName + " (codiceFiscale_Fisioterapista, codiceFiscale_Iscritto,durata, data, esito) values (?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, seduta.getCodiceFiscale_Fisioterapista());
            statement.setString(2, seduta.getCodiceFiscale_Iscritto());
            statement.setInt(3, seduta.getDurata());
            statement.setDate(4, new java.sql.Date(seduta.getData().getTime()));
            statement.setString(5, seduta.getEsito());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            }
        }
    }

    public Seduta findByPrimaryKey(String codiceFiscale_Fisioterapista, String codiceFiscale_Iscritto, Date data) {
        Seduta seduta = null;

        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from " + tableName + " where codiceFiscale_Fisioterapista=? and codiceFiscale_Iscritto=? and data=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceFiscale_Fisioterapista);
            statement.setString(2, codiceFiscale_Iscritto);
            statement.setDate(3, new java.sql.Date(data.getTime()));
            ResultSet result = statement.executeQuery();
            if (result.next()) {                
                seduta = new Seduta(); 
                seduta.setCodiceFiscale_Fisioterapista(result.getString("codiceFiscale_Fisioterapista"));
                seduta.setCodiceFiscale_Iscritto(result.getString("codiceFiscale_Iscritto"));
                seduta.setDurata(result.getInt("durata"));
                seduta.setData(result.getDate("data"));
                seduta.setEsito(result.getString("esito"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            }
        }
        return seduta;
    }
}
