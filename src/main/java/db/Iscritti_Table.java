package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import db.connection.DBConnection;
import db.model.Iscritto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Iscritti_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Iscritti_Table() {
        dataSource = new DBConnection();
        tableName="ISCRITTI";
    }
  
    
    public ObservableList<Iscritto> getData(){      
        ObservableList<Iscritto> list = FXCollections.observableArrayList();
        Connection connection = this.dataSource.getMySQLConnection();
        System.out.println(connection);
        PreparedStatement statement = null;
        String query = "select * from "+ tableName;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Iscritto iscritto = new Iscritto();
                iscritto.setCodiceFiscale(result.getString("codiceFiscale"));
                iscritto.setNome(result.getString("nome"));
                iscritto.setCognome(result.getString("cognome"));
                iscritto.setTelefono(result.getString("telefono"));
                iscritto.setDataNascita(result.getDate("dataNascita"));
                iscritto.setMail(result.getString("mail"));
                list.add(iscritto);
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return list;
    } 
    
    public void persist(Iscritto iscritto) {
        Connection connection = this.dataSource.getMySQLConnection();
        if (findByPrimaryKey(iscritto.getCodiceFiscale())!=null){
             new Exception("Student exists");
             System.out.println("Errore"+ "Student exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceFiscale, nome, cognome, telefono, dataNascita, mail) values (?,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, iscritto.getCodiceFiscale());
            statement.setString(2, iscritto.getNome());
            statement.setString(3, iscritto.getCognome());
            statement.setString(4, iscritto.getTelefono());
            statement.setDate(5, new java.sql.Date(iscritto.getDataNascita().getTime()));
            statement.setString(6, iscritto.getMail());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Iscritto findByPrimaryKey(String codiceFiscale)  {
        Iscritto iscritto = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceFiscale=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceFiscale);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                iscritto = new Iscritto();
                iscritto.setCodiceFiscale(result.getString("codiceFiscale"));
                iscritto.setNome(result.getString("nome"));
                iscritto.setCognome(result.getString("cognome"));
                iscritto.setTelefono(result.getString("telefono"));
                iscritto.setDataNascita(result.getDate("dataNascita"));
                iscritto.setMail(result.getString("mail"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return iscritto;
    } 
}
