package db;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Addetto_Pulizie;

public class Addetti_Pulizie_Table {
    private DBConnection dataSource;
    private String tableName;
    
    public Addetti_Pulizie_Table() {
        dataSource = new DBConnection();
        tableName="ADDETTI_PULIZIE";
    }
  
    public void persist(Addetto_Pulizie addetto) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(addetto.getCodiceFiscale())!=null){
             new Exception("Addetto delle pulizie exists");
             System.out.println("Errore"+ "addetto pulizie  exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceFiscale, nome, cognome, telefono, mail, pagaOraria) values (?,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, addetto.getCodiceFiscale());
            statement.setString(2, addetto.getNome());
            statement.setString(3, addetto.getCognome());
            statement.setString(4, addetto.getTelefono());
            statement.setString(5, addetto.getMail());
            statement.setInt(6, addetto.getPagaOraria());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Addetto_Pulizie findByPrimaryKey(String codiceFiscale)  {
        Addetto_Pulizie addetto = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceFiscale=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceFiscale);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                addetto = new Addetto_Pulizie();
                addetto.setCodiceFiscale(result.getString("codiceFiscale"));
                addetto.setNome(result.getString("nome"));
                addetto.setCognome(result.getString("cognome"));
                addetto.setTelefono(result.getString("telefono"));
                addetto.setMail(result.getString("mail"));
                addetto.setPagaOraria(result.getInt("pagaOraria"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return addetto;
    } 
}
