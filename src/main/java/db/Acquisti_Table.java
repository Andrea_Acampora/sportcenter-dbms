package db;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Acquisto;

public class Acquisti_Table {

    private DBConnection dataSource;
    private String tableName;
    
    public Acquisti_Table() {
        dataSource = new DBConnection();
        tableName="ACQUISTI";
    }
  
    public void persist(Acquisto acquisto) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(acquisto.getCodiceAcquisto())!=null){
             new Exception("Acquisto exists");
             System.out.println("Errore"+ "Acquisto exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceAcquisto, codiceTipologia, codiceArticolo, data, puntiGuadagnati, codiceFiscale) values (?,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1, acquisto.getCodiceAcquisto());
            statement.setString(2, acquisto.getCodiceTipologia());
            statement.setString(3, acquisto.getCodiceArticolo());
            statement.setDate(4, acquisto.getData());
            statement.setInt(5, acquisto.getPuntiGuadagnati());
            statement.setString(6, acquisto.getCodiceFiscale());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Acquisto findByPrimaryKey(int codiceAcquisto)  {
        Acquisto acquisto = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceAcquisto=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, codiceAcquisto);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                acquisto = new Acquisto();
                acquisto.setCodiceAcquisto(result.getInt("codiceAcquisto"));
                acquisto.setCodiceTipologia(result.getString("codiceTipologia"));
                acquisto.setCodiceArticolo(result.getString("codiceArticolo"));
                acquisto.setData(result.getDate("data"));
                acquisto.setPuntiGuadagnati(result.getInt("puntiGuadagnati"));
                acquisto.setCodiceFiscale(result.getString("codiceFiscale"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return acquisto;
    } 
}
