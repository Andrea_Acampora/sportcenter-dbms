package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.connection.DBConnection;
import db.model.Spogliatoio;

public class SpogliatoiTable {
    private DBConnection dataSource;
    private String tableName;
    
    public SpogliatoiTable() {
        dataSource = new DBConnection();
        tableName="SPOGLIATOI";
    }
    
    public void dropAndCreateTable() {
        Connection connection = this.dataSource.getMySQLConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try{
                statement.executeUpdate("DROP TABLE " + tableName);
            }
            catch (SQLException e) {
            }
            statement.executeUpdate (
                "create table `SPOGLIATOI` (\n" + 
                "     `idSpogliatio` VARCHAR(3) not null,\n" + 
                "     `superficie` INTEGER not null,\n" + 
                "     `numDocce` INTEGER not null,\n" + 
                "     constraint `IDSPOGLIATOIO` primary key (`idSpogliatio`));"
            );
            statement.close ();
        }
        catch (SQLException e) {
             new Exception(e.getMessage());
             System.out.println("Errore : "+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                 new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
    }
  
    public void persist(Spogliatoio spogliatoio) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(spogliatoio.getIdSpogliatoio())!=null){
             new Exception("Spogliatoio exists");
             System.out.println("Errore"+ "Student exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (idSpogliatoio, superficie, numDocce) values (?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, spogliatoio.getIdSpogliatoio());
            statement.setInt(2, spogliatoio.getSuperficie());
            statement.setInt(3, spogliatoio.getNumDocce());
            
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Spogliatoio findByPrimaryKey(String idSpogliatoio)  {
        Spogliatoio spogliatoio = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where idSpogliatoio=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, idSpogliatoio);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                spogliatoio = new Spogliatoio();                
                spogliatoio.setIdSpogliatoio(result.getString("idSpogliatoio"));
                spogliatoio.setSuperficie(result.getInt("superficie"));
                spogliatoio.setNumDocce(result.getInt("numDocce"));
                
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return spogliatoio;
    } 
}
