package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Abbonamento;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Abbonamenti_Table {

    private DBConnection dataSource;
    private String tableName;
    
    public Abbonamenti_Table() {
        dataSource = new DBConnection();
        tableName="ABBONAMENTI";
    }
  
    public ObservableList<Abbonamento> getData()  {
        ObservableList<Abbonamento> abbonamenti = FXCollections.observableArrayList();
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Abbonamento abbonamento = new Abbonamento();
                abbonamento.setCodiceAbbonamento(result.getInt("codiceAbbonamento"));
                abbonamento.setCodiceCorso(result.getString("codiceCorso"));
                abbonamento.setCodiceFiscale_Iscritto(result.getString("codiceFiscale_Iscritto"));
                abbonamento.setDataSottoscrizione(result.getDate("dataSottoscrizione"));
                abbonamento.setInizioValidita(result.getDate("inizioValidita"));
                abbonamento.setDurata(result.getInt("durata"));
                abbonamenti.add(abbonamento);
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return abbonamenti;
    } 
    
    public void persist(Abbonamento abbonamento) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(abbonamento.getCodiceAbbonamento())!=null){
             new Exception("abbonamento exists");
             System.out.println("Errore"+ "abbonamento exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (durata, codiceAbbonamento, dataSottoscrizione, inizioValidita, codiceFiscale_Iscritto, codiceCorso) values (?,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1, abbonamento.getDurata());
            statement.setInt(2, abbonamento.getCodiceAbbonamento());
            statement.setDate(3, abbonamento.getDataSottoscrizione());
            statement.setDate(4, abbonamento.getInizioValidita());
            statement.setString(5, abbonamento.getCodiceFiscale_Iscritto());
            statement.setString(5, abbonamento.getCodiceCorso());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Abbonamento findByPrimaryKey(int codiceAbbonamento)  {
        Abbonamento abbonamento = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceAbbonamento=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, codiceAbbonamento);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                abbonamento = new Abbonamento();
                abbonamento.setCodiceAbbonamento(result.getInt("codiceAbbonamento"));
                abbonamento.setCodiceCorso(result.getString("codiceCorso"));
                abbonamento.setCodiceFiscale_Iscritto(result.getString("codiceFiscale_Iscritto"));
                abbonamento.setDataSottoscrizione(result.getDate("dataSottoscrizione"));
                abbonamento.setInizioValidita(result.getDate("inizioValidita"));
                abbonamento.setDurata(result.getInt("durata"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return abbonamento;
    } 
}
