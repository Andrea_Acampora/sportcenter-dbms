package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import db.connection.DBConnection;
import db.model.TipologiaProdotto;

public class TipologieProdottiTable {
    private DBConnection dataSource;
    private String tableName;
    
    public TipologieProdottiTable() {
        dataSource = new DBConnection();
        tableName="TIPOLOGIE_PRODOTTI";
    }
    
  
    public void persist(TipologiaProdotto tipologiaProdotto) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(tipologiaProdotto.getCodiceTipologia())!=null){
             new Exception("tipologie prodotti exists");
             System.out.println("Errore"+ "tipologie prodotti exists");
        }
        
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceTipologia, prezzo, descrizione) values (?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, tipologiaProdotto.getCodiceTipologia()); 
            statement.setInt(2, tipologiaProdotto.getPrezzo());
            statement.setString(3, tipologiaProdotto.getDescrizione());
            statement.executeUpdate();
                        
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public TipologiaProdotto findByPrimaryKey(String codiceTipologia)  {
        TipologiaProdotto tipologiaProdotto = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceTipologia=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceTipologia);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                tipologiaProdotto = new TipologiaProdotto();                                
            }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return tipologiaProdotto;
    } 
}
