package db.model;

import java.util.Date;

public class Seduta {

    private String codiceFiscale_Fisioterapista;
    private String codiceFiscale_Iscritto;
    private int durata;
    private Date data;
    private String esito;

    public String getCodiceFiscale_Fisioterapista() {
        return codiceFiscale_Fisioterapista;
    }

    public void setCodiceFiscale_Fisioterapista(String codiceFiscale_Fisioterapista) {
        this.codiceFiscale_Fisioterapista = codiceFiscale_Fisioterapista;
    }

    public String getCodiceFiscale_Iscritto() {
        return codiceFiscale_Iscritto;
    }

    public void setCodiceFiscale_Iscritto(String codiceFiscale_Iscritto) {
        this.codiceFiscale_Iscritto = codiceFiscale_Iscritto;
    }

    public int getDurata() {
        return durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

}
