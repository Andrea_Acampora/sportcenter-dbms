package db.model;

import java.sql.Date;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Abbonamento {
    
       private ObjectProperty<Integer> durata;
       private ObjectProperty<Integer> codiceAbbonamento;
       private StringProperty codiceFiscale_Iscritto;
       private ObjectProperty<Date> dataSottoscrizione;
       private ObjectProperty<Date> inizioValidita;
       private StringProperty codiceCorso;
   
   
       public Abbonamento() {
           this.durata = new SimpleObjectProperty<Integer>(this,"durata");
           this.codiceAbbonamento = new SimpleObjectProperty<Integer>(this,"codice");
           this.codiceFiscale_Iscritto = new SimpleStringProperty(this,"codiceFiscale");
           this.dataSottoscrizione = new SimpleObjectProperty<Date>(this,"dataSottoscrizione");
           this.inizioValidita = new SimpleObjectProperty<Date>(this,"inizioValidita");
           this.codiceCorso = new SimpleStringProperty(this,"codiceCorso");

       }
       
       public StringProperty codiceFiscale_IscrittoProperty() {
           return this.codiceFiscale_Iscritto;
       }
       
       public StringProperty codiceCorsoProperty() {
           return this.codiceCorso;
       }
       public ObjectProperty<Integer> durataProperty() {
           return this.durata;
       }
       public ObjectProperty<Integer> codiceAbbonamentoProperty() {
           return this.codiceAbbonamento;
       }
       public ObjectProperty<Date> dataSottoscrizioneProperty() {
           return this.dataSottoscrizione;
       }
       
       public ObjectProperty<Date> inizioValiditaProperty() {
           return this.inizioValidita;
       }
           
        public int getDurata() {
            return durata.get();
        }
        
        public void setDurata(int durata) {
            this.durata.set(durata);
        }
        public int getCodiceAbbonamento() {
            return codiceAbbonamento.get();
        }
        public void setCodiceAbbonamento(int codiceAbbonamento) {
            this.codiceAbbonamento.set(codiceAbbonamento);
        }
        public String getCodiceFiscale_Iscritto() {
            return codiceFiscale_Iscritto.get();
        }
        public void setCodiceFiscale_Iscritto(String codiceFiscale_Iscritto) {
            this.codiceFiscale_Iscritto.set(codiceFiscale_Iscritto);
        }
        public Date getDataSottoscrizione() {
            return dataSottoscrizione.get();
        }
        public void setDataSottoscrizione(Date dataSottoscrizione) {
            this.dataSottoscrizione.set(dataSottoscrizione);
        }
        public String getCodiceCorso() {
            return codiceCorso.get();
        }
        public void setCodiceCorso(String codiceCorso) {
            this.codiceCorso.set(codiceCorso);
        }

        public Date getInizioValidita() {
            return inizioValidita.get();
        }

        public void setInizioValidita(Date inizioValidita) {
            this.inizioValidita.set(inizioValidita);
        }
}

