package db.model;

public class Sport {

    private String codiceSport;
    private String nome;

    public String getCodiceSport() {
        return codiceSport;
    }

    public void setCodiceSport(String codiceSport) {
        this.codiceSport = codiceSport;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
