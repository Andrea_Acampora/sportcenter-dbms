package db.model;

public class Allenatori_Corsi {

    private String codiceFiscale_Allenatore;
    private String codiceCorso;
   
    
    public Allenatori_Corsi() {
    }


    public String getCodiceFiscale_Allenatore() {
        return codiceFiscale_Allenatore;
    }


    public void setCodiceFiscale_Allenatore(String codiceFiscale_Allenatore) {
        this.codiceFiscale_Allenatore = codiceFiscale_Allenatore;
    }


    public String getCodiceCorso() {
        return codiceCorso;
    }


    public void setCodiceCorso(String codiceCorso) {
        this.codiceCorso = codiceCorso;
    }
    
    
    
}
