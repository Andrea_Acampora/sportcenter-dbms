package db.model;

public class Fascia_Oraria {

    private String idFascia;
    private int ora_inizio;
    private int ora_fine;
   
    
    public Fascia_Oraria() {
    }


    public String getIdFascia() {
        return idFascia;
    }


    public void setIdFascia(String idFascia) {
        this.idFascia = idFascia;
    }


    public int getOra_inizio() {
        return ora_inizio;
    }


    public void setOra_inizio(int ora_inizio) {
        this.ora_inizio = ora_inizio;
    }


    public int getOra_fine() {
        return ora_fine;
    }


    public void setOra_fine(int ora_fine) {
        this.ora_fine = ora_fine;
    }

    
}
