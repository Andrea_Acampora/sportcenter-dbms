package db.model;

import java.sql.Date;

public class OccupazioneCampo {

    private String codiceCampo;
    private String idFascia;
    private Date data;
    private String codiceCorso; 
    private String codiceFiscale_Iscritto;

    public void setCodiceCampo(String codiceCampo) {
        this.codiceCampo = codiceCampo;
    }

    public void setIdFascia(String idFascia) {
        this.idFascia = idFascia;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setCodiceFiscale_Iscritto(String codiceFiscale_Iscritto) {
        this.codiceFiscale_Iscritto = codiceFiscale_Iscritto;
    }

    public String getCodiceCampo() {
        return codiceCampo;
    }

    public String getIdFascia() {
        return idFascia;
    }

    public Date getData() {
        return data;
    }

    public String getCodiceFiscale_Iscritto() {
        return codiceFiscale_Iscritto;
    }

    public String getCodiceCorso() {
        return codiceCorso;
    }

    public void setCodiceCorso(String codiceCorso) {
        this.codiceCorso = codiceCorso;
    }

}
