package db.model;

public class Corso {

    private String codiceCorso;
    private String codiceSport;
    private int eta_partecipanti;
    private int max_partecipanti;
    private String descrizione;   
    
    public Corso() {
    }


    public String getCodiceCorso() {
        return codiceCorso;
    }


    public void setCodiceCorso(String codiceCorso) {
        this.codiceCorso = codiceCorso;
    }


    public String getCodiceSport() {
        return codiceSport;
    }


    public void setCodiceSport(String codiceSport) {
        this.codiceSport = codiceSport;
    }


    public int getEta_partecipanti() {
        return eta_partecipanti;
    }


    public void setEta_partecipanti(int eta_partecipanti) {
        this.eta_partecipanti = eta_partecipanti;
    }


    public int getMax_partecipanti() {
        return max_partecipanti;
    }


    public void setMax_partecipanti(int max_partecipanti) {
        this.max_partecipanti = max_partecipanti;
    }


    public String getDescrizione() {
        return descrizione;
    }


    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
}
