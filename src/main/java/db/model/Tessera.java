package db.model;

public class Tessera {

    private int numTessera;
    private String codiceFiscale_Iscritto;
    private int punti;

    public int getNumTessera() {
        return numTessera;
    }

    public void setNumTessera(int numTessera) {
        this.numTessera = numTessera;
    }

    public String getCodiceFiscale_Iscritto() {
        return codiceFiscale_Iscritto;
    }

    public void setCodiceFiscale_Iscritto(String codiceFiscale_Iscritto) {
        this.codiceFiscale_Iscritto = codiceFiscale_Iscritto;
    }

    public int getPunti() {
        return punti;
    }

    public void setPunti(int punti) {
        this.punti = punti;
    }

}
