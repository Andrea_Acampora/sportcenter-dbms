package db.model;

public class Campo {

    private String codiceCampo;
    private int superficie;
    private int costoOrario;
    private String tipologiaCampo;
    private String idSpogliatoio;
   
    
    public Campo() {
    }


    public String getCodiceCampo() {
        return codiceCampo;
    }


    public void setCodiceCampo(String codiceCampo) {
        this.codiceCampo = codiceCampo;
    }


    public int getSuperficie() {
        return superficie;
    }


    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }


    public int getCostoOrario() {
        return costoOrario;
    }


    public void setCostoOrario(int costoOrario) {
        this.costoOrario = costoOrario;
    }


    public String getTipologiaCampo() {
        return tipologiaCampo;
    }


    public void setTipologiaCampo(String tipologiaCampo) {
        this.tipologiaCampo = tipologiaCampo;
    }


    public String getIdSpogliatoio() {
        return idSpogliatoio;
    }


    public void setIdSpogliatoio(String idSpogliatoio) {
        this.idSpogliatoio = idSpogliatoio;
    }

}
