package db.model;

public class Armadietto {

    private String idSpogliatoio;
    private int numero;
   
    
    public Armadietto() {
    }


    public String getIdSpogliatoio() {
        return idSpogliatoio;
    }


    public void setIdSpogliatoio(String idSpogliatoio) {
        this.idSpogliatoio = idSpogliatoio;
    }


    public int getNumero() {
        return numero;
    }


    public void setNumero(int numero) {
        this.numero = numero;
    } 
}
