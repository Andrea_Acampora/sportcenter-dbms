package db.model;

public class TipologiaAbbonamento {

    private int durata;
    private int prezzo;
    private int puntiOttenuti;

    public int getDurata() {
        return durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public int getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(int prezzo) {
        this.prezzo = prezzo;
    }

    public int getPuntiOttenuti() {
        return puntiOttenuti;
    }

    public void setPuntiOttenuti(int puntiOttenuti) {
        this.puntiOttenuti = puntiOttenuti;
    }

}
