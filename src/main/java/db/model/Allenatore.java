package db.model;

import java.util.Date;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Allenatore {
    
    private StringProperty codiceFiscale;
    private StringProperty nome;
    private StringProperty cognome;
    private StringProperty telefono;
    private StringProperty mail;
    private ObjectProperty<Integer> pagaOraria;
    private StringProperty gradoPatentino;


    public Allenatore(){
        this.codiceFiscale = new SimpleStringProperty("codiceFiscale");
        this.nome = new SimpleStringProperty("nome");
        this.cognome  = new SimpleStringProperty("cognome");
        this.telefono = new SimpleStringProperty("telefono");
        this.mail = new SimpleStringProperty("mail");
        this.gradoPatentino = new SimpleStringProperty("gradoPatentino"); 
        this.pagaOraria = new SimpleObjectProperty<Integer>(this, "pagaOraria"); 
    }

    public StringProperty codiceFiscaleProperty() {
        return this.codiceFiscale;
    }
    
    public StringProperty nomeProperty() {
        return this.nome;
    }
    
    public StringProperty cognomeProperty() {
        return this.cognome;
    }
    
    public StringProperty telefonoProperty() {
        return this.telefono;
    }
    
    public StringProperty mailProperty() {
        return this.mail;
    }
    
    public ObjectProperty<Integer> pagaOrariaProperty() {
        return this.pagaOraria;
    }
    
    public StringProperty gradoPatentinoProperty() {
        return this.gradoPatentino;
    }
    
    public String getCodiceFiscale() {
        return codiceFiscale.get();
    }


    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale.set(codiceFiscale);
    }


    public String getNome() {
        return nome.get();
    }


    public void setNome(String nome) {
        this.nome.set(nome);
    }


    public String getCognome() {
        return cognome.get(); 
    }


    public void setCognome(String cognome) {
        this.cognome.set(cognome);
    }


    public String getTelefono() {
        return telefono.get();
    }


    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }


    public String getMail() {
        return mail.get(); 
    }


    public void setMail(String mail) {
        this.mail.set(mail);
    }


    public int getPagaOraria() {
        return pagaOraria.get(); 
    }

    public void setPagaOraria(int pagaOraria) {
        this.pagaOraria.set(pagaOraria);
    }


    public String getGradoPatentino() {
        return gradoPatentino.get();
    }


    public void setGradoPatentino(String gradoPatentino) {
        this.gradoPatentino.set(gradoPatentino);
    }
}
