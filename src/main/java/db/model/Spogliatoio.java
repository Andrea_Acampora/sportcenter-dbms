package db.model;

public class Spogliatoio {

    private String idSpogliatoio;
    private int superficie;
    private int numDocce;

    public String getIdSpogliatoio() {
        return idSpogliatoio;
    }

    public void setIdSpogliatoio(String idSpogliatoio) {
        this.idSpogliatoio = idSpogliatoio;
    }

    public int getSuperficie() {
        return superficie;
    }

    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }

    public int getNumDocce() {
        return numDocce;
    }

    public void setNumDocce(int numDocce) {
        this.numDocce = numDocce;
    }

}
