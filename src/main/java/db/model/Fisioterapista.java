package db.model;

public class Fisioterapista {

    private String codiceFiscale;
    private String nome;
    private String cognome;
    private String telefono;
    private String mail;
    private int pagaOraria;
    private String specializzazione;
    private int annoAbilitazione;

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getPagaOraria() {
        return pagaOraria;
    }

    public void setPagaOraria(int pagaOraria) {
        this.pagaOraria = pagaOraria;
    }

    public String getSpecializzazione() {
        return specializzazione;
    }

    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }

    public int getAnnoAbilitazione() {
        return annoAbilitazione;
    }

    public void setAnnoAbilitazione(int annoAbilitazione) {
        this.annoAbilitazione = annoAbilitazione;
    }

}
