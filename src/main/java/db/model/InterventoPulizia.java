package db.model;

import java.util.Date;

public class InterventoPulizia {

    private String idSpogliatoio;
    private String codiceFiscale_Addetto;
    private int durata;
    private Date data; 

    public String getIdSpogliatoio() {
        return idSpogliatoio;
    }

    public String getCodiceFiscale_Addetto() {
        return codiceFiscale_Addetto;
    }

    public int getDurata() {
        return durata;
    }

    public void setIdSpogliatoio(String idSpogliatoio) {
        this.idSpogliatoio = idSpogliatoio;
    }

    public void setCodiceFiscale_Addetto(String codiceFiscale_Addetto) {
        this.codiceFiscale_Addetto = codiceFiscale_Addetto;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

}
