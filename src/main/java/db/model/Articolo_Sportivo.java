package db.model;

public class Articolo_Sportivo {

    private String codiceTipologia;
    private String codiceArticolo;
    private String taglia;
   
    
    public Articolo_Sportivo() {
    }


    public String getCodiceTipologia() {
        return codiceTipologia;
    }


    public void setCodiceTipologia(String codiceTipologia) {
        this.codiceTipologia = codiceTipologia;
    }


    public String getCodiceArticolo() {
        return codiceArticolo;
    }


    public void setCodiceArticolo(String codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
    }


    public String getTaglia() {
        return taglia;
    }


    public void setTaglia(String taglia) {
        this.taglia = taglia;
    }
}
