package db.model;

public class TipologiaProdotto {

    private String codiceTipologia;
    private int prezzo;
    private String descrizione;

    public String getCodiceTipologia() {
        return codiceTipologia;
    }

    public void setCodiceTipologia(String codiceTipologia) {
        this.codiceTipologia = codiceTipologia;
    }

    public int getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(int prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

}
