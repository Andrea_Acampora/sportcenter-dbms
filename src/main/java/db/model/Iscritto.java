package db.model;

import java.util.Date;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Iscritto {
    
    private StringProperty codiceFiscale; 
    private StringProperty nome;
    private StringProperty cognome;
    private StringProperty telefono ;
    private ObjectProperty<Date> dataNascita ; 
    private StringProperty mail; 


    public Iscritto(){
        this.codiceFiscale = new SimpleStringProperty("codiceFiscale");
        this.nome = new SimpleStringProperty("nome");
        this.cognome  = new SimpleStringProperty("cognome");
        this.telefono = new SimpleStringProperty("telefono");
        this.mail = new SimpleStringProperty("mail");
        this.dataNascita = new SimpleObjectProperty<Date>(this,"dataNascita");
    }
    
    public String getCodiceFiscale() {
        return codiceFiscale.get();
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale.set(codiceFiscale);
    }
    
    public StringProperty codiceFiscaleProperty() {
        return this.codiceFiscale;
    }


    public String getNome() {
        return nome.get();
    }


    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    public StringProperty nomeProperty() {
        return this.nome;
    }


    public String getCognome() {
        return cognome.get();
    }


    public void setCognome(String cognome) {
        this.cognome.set(cognome);
    }

    public StringProperty cognomeProperty() {
        return this.cognome;
    }

    public String getTelefono() {
        return telefono.get();
    }


    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    public StringProperty telefonoProperty() {
        return this.telefono;
    }

    public String getMail() {
        return mail.get();
    }


    public void setMail(String mail) {
        this.mail.set(mail);
    }

    public StringProperty mailProperty() {
        return this.mail;
    }
    
    public Date getDataNascita() {
        return dataNascita.get();
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita.set(dataNascita);
    }
    
    public ObjectProperty<Date> dataNascitaProperty() {
        return this.dataNascita;
    }
    
}
