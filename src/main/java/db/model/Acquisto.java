package db.model;

import java.sql.Date;

public class Acquisto {
    
   private int codiceAcquisto;
   private String codiceTipologia;
   private String codiceArticolo;
   private Date data;
   private int puntiGuadagnati;
   private String codiceFiscale;
   
    public int getCodiceAcquisto() {
        return codiceAcquisto;
    }
    public void setCodiceAcquisto(int codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }
    public String getCodiceTipologia() {
        return codiceTipologia;
    }
    public void setCodiceTipologia(String codiceTipologia) {
        this.codiceTipologia = codiceTipologia;
    }
    public String getCodiceArticolo() {
        return codiceArticolo;
    }
    public void setCodiceArticolo(String codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
    }
    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }
    public int getPuntiGuadagnati() {
        return puntiGuadagnati;
    }
    public void setPuntiGuadagnati(int puntiGuadagnati) {
        this.puntiGuadagnati = puntiGuadagnati;
    }
    public String getCodiceFiscale() {
        return codiceFiscale;
    }
    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }
}

