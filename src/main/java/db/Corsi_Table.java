package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import db.connection.DBConnection;
import db.model.Corso;

public class Corsi_Table {

    private DBConnection dataSource;
    private String tableName;
    
    public Corsi_Table() {
        dataSource = new DBConnection();
        tableName="CORSI";
    }
  
    public void persist(Corso corso) {
        Connection connection = this.dataSource.getMySQLConnection();

        if (findByPrimaryKey(corso.getCodiceCorso())!=null){
             new Exception("Corso exists");
             System.out.println("Errore"+ "corso exists");
        }
        PreparedStatement statement = null; 
        String insert = "insert into "+ tableName +" (codiceCorso, codiceSport, descrizione, eta_partecipanti, max_partecipanti) values (?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, corso.getCodiceCorso());
            statement.setString(2, corso.getCodiceSport());
            statement.setString(3, corso.getDescrizione());
            statement.setInt(4, corso.getEta_partecipanti());
            statement.setInt(5, corso.getMax_partecipanti());
            statement.executeUpdate();
        }

        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Corso findByPrimaryKey(String codiceCorso)  {
        Corso corso = null;
        
        Connection connection = this.dataSource.getMySQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where codiceCorso=?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, codiceCorso);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                corso = new Corso();
                corso.setCodiceCorso(result.getString("codiceCorso"));
                corso.setCodiceSport(result.getString("codiceSport"));
                corso.setDescrizione(result.getString("descrizione"));
                corso.setEta_partecipanti(result.getInt("eta_partecipanti"));
                corso.setMax_partecipanti(result.getInt("max_partecipanti"));
                }
        }
        catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                 System.out.println("Errore"+ e.getMessage());
            }
        }
        return corso;
    } 
}
